var router = require('express').Router();
var validate = require('express-validation');

var repository = require('./quiz.repository.js');
var quizAnswerChecker = require('./quizAnswerChecker.js');
var submitResultValidation = require('./submitUserResult.validator.js');

function getQuizById(req, res, next) {
    repository.findQuizById(req.params.id, function(err, result) {
        if(err) {
            return next(err);
        }
        res.json(result);
    })    
}

function getListOfQuizes(req, res, next) {
    repository.findAllQuizes(function(err, result) {
        if(err) {
            return next(err);
        }
        res.json(result);
    });
}

function checkUserAnswers(req, res, next) {
    var resultCallback = function(err, result) { 
        if(err) {
            return next(err);
        }
        res.json(result); 
    };
    quizAnswerChecker.checkUserAnswers(
        req.body.answers, 
        req.body.quizId, 
        req.body.username, 
        resultCallback
    );
}

router.get('/quiz/list', getListOfQuizes);
router.get('/quiz/:id', getQuizById);
router.post('/result', validate(submitResultValidation.body), checkUserAnswers);

module.exports = router;