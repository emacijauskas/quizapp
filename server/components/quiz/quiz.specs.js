var sinon = require('sinon');
var chai = require('chai');

beforeEach(function() {
    this.sandbox = sinon.sandbox.create();
});

afterEach(function() {
    this.sandbox.restore();
});


var repository = require('./quiz.repository');
var quizAnswerChecker = require('./quizAnswerChecker')

var expect = require('chai').expect;

describe('Quiz answer checker', function() {
    it('returns all answers as incorrect', function(){
        
        var quizId = 2;
        var userAnswers = [
            { questionId: 2, selectedAnswer: 1},
            { questionId: 1, selectedAnswer: 1}            
        ];

        var answers = { 
            correctAnswers: [
            { questionId: 1, correctAnswerId: 3},
            { questionId: 2, correctAnswerId: 8}            
        ]};

        var repositoryStub = this.sandbox.stub(repository, 'findQuizAnswers', function(id, cb) {
            cb(null, answers);
        });

        quizAnswerChecker.checkUserAnswers(userAnswers, quizId, 'test', function(err, res) {
            expect(res[0].isCorrect).to.be.false;
            expect(res[1].isCorrect).to.be.false;
        });

        expect(repositoryStub.calledWith(quizId)).to.be.ok;
    });

    it('returns all answers as correct', function(){
        
        var quizId = 2;
        var userAnswers = [
            { questionId: 1, selectedAnswer: 3},
            { questionId: 2, selectedAnswer: 8}            
        ];

        var answers = { 
            correctAnswers: [
            { questionId: 1, correctAnswerId: 3},
            { questionId: 2, correctAnswerId: 8}            
        ]};

        var repositoryStub = this.sandbox.stub(repository, 'findQuizAnswers', function(id, cb) {
            cb(null, answers);
        });

        quizAnswerChecker.checkUserAnswers(userAnswers, quizId, 'test', function(err, res) {
            expect(res[0].isCorrect).to.be.true;
            expect(res[1].isCorrect).to.be.true;
        });

        expect(repositoryStub.calledWith(quizId)).to.be.ok;
    })

    it('takes only required amount of answers according to specific quiz answers', function(){
        
        var quizId = 2;
        var userAnswers = [
            { questionId: 1, selectedAnswer: 3},
            { questionId: 3, selectedAnswer: 2},         
            { questionId: 5, selectedAnswer: 5},           
            { questionId: 2, selectedAnswer: 4},           
            { questionId: 6, selectedAnswer: 8},           
        ];

        var answers = { 
            correctAnswers: [
            { questionId: 1, correctAnswerId: 3},
            { questionId: 2, correctAnswerId: 8}            
        ]};

        var repositoryStub = this.sandbox.stub(repository, 'findQuizAnswers', function(id, cb) {
            cb(null, answers);
        });

        quizAnswerChecker.checkUserAnswers(userAnswers, quizId, 'test', function(err, res) {
            expect(res).to.have.lengthOf(answers.correctAnswers.length);
        });
    })
});