var Joi = require('joi');

module.exports = { 
    body: {
        username: Joi.string().min(3).max(25).required(),
        quizId: Joi.string().required(),
        answers: Joi.array({
                questionId: Joi.number().integer().required(), 
                selectedAnswer: Joi.number().integer().required() 
            }).required().min(1)
    }
}
