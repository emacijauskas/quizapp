var repository = require('./quiz.repository.js');

var quizAnswersChecker = {
    checkUserAnswers: function(answers, quizId, username, callback) {
        repository.findQuizAnswers(quizId, function(err, result) {
            if(result) {
                var results = [];

                var sortedExpectedAnswers = sortAnswersByQuestionId(result.correctAnswers);
                var slicedUserAnswers = takeExpectedCountOfAnswers(answers, sortedExpectedAnswers.length);
                var sortedUserAnswers = sortAnswersByQuestionId(slicedUserAnswers); 

                var isCorrect = false;
                for(var i=0; i < sortedExpectedAnswers.length; i++) {
                    if(sortedUserAnswers[i].questionId === sortedExpectedAnswers[i].questionId) {
                        isCorrect = isAnswerCorrect(sortedUserAnswers[i].selectedAnswer, sortedExpectedAnswers[i].correctAnswerId);
                    }

                    results.push({
                        questionId: sortedExpectedAnswers[i].questionId, 
                        isCorrect: isCorrect  
                    });

                    isCorrect = false;
                }

                repository.insertUserResult(results, username, quizId, function(err) {
                    if(err) {
                        console.log(err);
                    }
                });

                callback(null, results);
            }
        });
    }
}

function sortAnswersByQuestionId(answers) {
    return answers.sort(function(a, b) {
        return a.questionId-b.questionId;
    });
}

function takeExpectedCountOfAnswers(answers, totalAnswersExpected) {
    return answers.slice(0, totalAnswersExpected);
}

function isAnswerCorrect(userGuess, correctAnswer) {
    return userGuess === correctAnswer;
}

module.exports = quizAnswersChecker;