var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');

var port = process.env.PORT || 9000;
var app = express();

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
}

app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    next();
});

// routing
app.use(require('./components/quiz/quiz.router.js'));
//

app.listen(port, () => {
    console.log('listening on port', port);
})