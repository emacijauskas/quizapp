# Quiz app #

Requires mongodb

### Steps to run locally ###

1. Install mongodb (uses default configuration).
2. Install server packages. `npm install`
3. Install client packages. `cd client` and `npm install`
4. Run at the root `npm run start`.
5. Insert sample quiz using mongo shell (collection: `quizes`). 
(you can find quiz sample `/server/quiz.model.json`)