import React, { Component } from 'react'

export default class MainView extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                        {this.props.children}
                    </div>
                    <div className="col-md-4"></div>
                </div>
            </div>
        )
    }
}