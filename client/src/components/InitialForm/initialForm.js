import React, { Component } from 'react';
import { browserHistory } from 'react-router';

import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import './initialForm.css';

import QuizRepository from './../Quiz/quizRepository';
import Asterisk from './../_shared/Asterisk/asterisk';

export default class InitialForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            quizIdSelected: '',
            listOfQuizes: [],
            usernameIsValid: false,
        };

        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handleQuizSelection = this.handleQuizSelection.bind(this);
        this.handleUsernameInput = this.handleUsernameInput.bind(this);
    }

    componentDidMount() {
        QuizRepository.getAllQuizes()
            .then(quizes => this.setState({ listOfQuizes: quizes }));
    }

    handleFormSubmit(event) {
        event.preventDefault();
        browserHistory.push(`/quiz/${this.state.quizIdSelected}/${this.state.username}`);
    }

    handleQuizSelection(event) {
        this.setState({ quizIdSelected: event.target.value });
    }

    handleUsernameInput(event) {
        var input = event.target.value;
        this.setState({ username: input });

        var isValid = input.length > 2 ? true : false;
        this.setState({ usernameIsValid:  isValid });
    }

    render() {
        return (
            <div>
                <div className="Quiz-header">
                    <h2 className="InitialForm-title">Wiz quiz</h2>
                </div>

                <p className="Quiz-about">
                    This is a quiz app. Solve me and you will be rewarded with nothing!!
                </p>
                
                <form onSubmit={this.handleFormSubmit}>

                    <FormGroup controlId="formInlineName">
                        <ControlLabel>
                            <Asterisk/>
                            Your name (at least 3 characters):
                        </ControlLabel>
                        <FormControl 
                            type="text" 
                            placeholder="e.g Jesus" 
                            value={this.state.username} 
                            onChange={this.handleUsernameInput}/>
                    </FormGroup>

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>
                            <Asterisk/>
                            Select quiz:
                        </ControlLabel>
                        <FormControl 
                            componentClass="select" 
                            placeholder="select" 
                            value={this.state.quizIdSelected} 
                            onChange={this.handleQuizSelection}><option value="">..</option>
                            {
                                this.state.listOfQuizes.map((quiz) =>
                                    <option key={quiz._id} value={quiz._id}>{quiz.title}</option>
                                )
                            }
                        </FormControl>
                    </FormGroup>

                    <Button
                        type="submit" 
                        bsStyle="primary" 
                        bsSize="large" 
                        className="Quiz-button-start"
                        disabled={!this.state.quizIdSelected || !this.state.usernameIsValid }>Start quiz
                    </Button>

                </form>
            </div>
        )
    }
}