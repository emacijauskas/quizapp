import React, { Component } from 'react';

import './loader.css';

export default class Loader extends Component {
    render() {
        return (
            <div>
                { this.props.hasLoaded ? this.props.children : <p className="Loader">Loading..</p> }
            </div>
        )
    }
}