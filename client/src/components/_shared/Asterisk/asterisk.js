import React, { Component } from 'react';

import './asterisk.css';

export default class Asterisk extends Component {
    render() {
        return (
            <span className="Asterisk">*</span>
        )
    }
}