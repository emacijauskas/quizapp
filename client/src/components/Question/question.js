import React, { Component } from 'react';

import { FormGroup, Radio } from 'react-bootstrap';
import './question.css';

export default class Question extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            selectedAnswer: 0,
            questionId : this.props.content.id
        };

        this.submitAnswer = this.submitAnswer.bind(this);
    }

    submitAnswer(event) {
        var answerId = Number(event.target.value);
        this.setState({ selectedAnswer: answerId });
        this.props.onQuestionSubmit({ selectedAnswer: answerId, questionId: this.state.questionId });
    }
    
    render() {
        return (
            <div className="Question">
                <p>{this.props.content.question}</p>
                <form>
                    <FormGroup>
                    {
                        this.props.content.answers.map((answer, index) =>
                            <Radio 
                                type="radio" 
                                key={answer.id} 
                                value={answer.id}
                                className="Answer-radio" 
                                onChange={this.submitAnswer}
                                checked={this.state.selectedAnswer === Number(answer.id)}>
                                {answer.text}
                            </Radio> 
                        )
                    }
                    </FormGroup>
                </form>
            </div>
        )
    }
}