import React, { Component } from 'react';
import { Link } from 'react-router';

import { Button, Table } from 'react-bootstrap';
import './results.css';

export default class Results extends Component {
    render() {
        return (
            <div className="Results">
                <h4>Results:</h4>
                <Table bordered condensed hover className="Results-table">
                    <thead>
                        <tr>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.props.results.map((result, index) => 
                                <tr 
                                    key={result.questionId} 
                                    className={result.isCorrect ? 'success' : 'danger'}
                                >
                                    <td>{index+1}</td>
                                </tr>
                            )
                        }
                    </tbody>
                </Table>
                <Button className="Results-button-try" bsStyle="primary" onClick={this.props.restartQuiz}>Try again!</Button>
                <Link className="Results-button-home" to="/">Go home</Link>
            </div>
        )
    }
}