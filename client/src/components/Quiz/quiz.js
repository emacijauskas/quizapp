import React, { Component } from 'react';

import { Badge } from 'react-bootstrap';
import './quiz.css';

import QuizRepository from './quizRepository';
import Question from './../Question/question';
import Results from './../Results/results';
import Loader from './../_shared/Loader/loader';

export default class Quiz extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hasLoaded: false,
            quiz: { 
                questions: []
            },
            answers: [], 
            currentQuestionIndex: 0,
            showResults: false,
            results: []
        };

        this.baseState = this.state;
        this.setQuestionAnswer = this.setQuestionAnswer.bind(this);
        this.resetProgress = this.resetProgress.bind(this);
        this.resetResultStatus = this.resetResultStatus.bind(this);
        this.submitUserAnswers = this.submitUserAnswers.bind(this);
    }

    componentDidMount() {
        QuizRepository.getQuizById(this.props.params.id)
            .then(quiz => 
                { 
                    this.setState({ quiz: quiz });
                    this.setState({ hasLoaded: true });
                });
    }

    setQuestionAnswer(answer) {
        var updatedAnswers = this.state.answers;
        updatedAnswers.push(answer);
        this.setState({ answers: updatedAnswers });

        if(this.state.currentQuestionIndex + 1 === this.state.quiz.questions.length) {
            this.submitUserAnswers();
        } else {
            var newQuestionIndex = this.state.currentQuestionIndex + 1;
            this.setState({ currentQuestionIndex: newQuestionIndex });
        }
    }

    submitUserAnswers() {
        this.setState({ hasLoaded: false });
        var userAnswers = { 
            answers: this.state.answers, 
            quizId: this.props.params.id, 
            username: this.props.params.username 
        };
        QuizRepository.submitAnswers(userAnswers)
            .then((results) => {
                this.resetProgress();
                this.setState({ results: results });
                this.setState({ showResults: true });
                this.setState({ hasLoaded: true });
            }
        );
    }

    resetProgress() {
        this.setState({
            answers: [], 
            currentQuestionIndex: 0,
        });
    }

    resetResultStatus() {
        this.setState({ showResults: false });
    }

    render() {
        var prebuiltQuestions = this.state.quiz.questions.map((question, index) => 
             <Question key={index} content={question} onQuestionSubmit={this.setQuestionAnswer} />);
        return (
            <Loader hasLoaded={this.state.hasLoaded}>
                <div className="Quiz">
                    <div>
                        <h3 className="Quiz-title">{this.state.quiz.title}</h3>
                        {this.state.showResults || <Badge pullRight>#{this.state.currentQuestionIndex + 1}/{this.state.quiz.questions.length}</Badge>}
                    </div>
                    {this.state.showResults ? <Results results={this.state.results} restartQuiz={this.resetResultStatus} /> : prebuiltQuestions[this.state.currentQuestionIndex]}
                </div>
            </Loader>
        )
    }
}