import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

import MainView from './components/MainView/mainView';
import InitialForm from './components/InitialForm/initialForm';
import Quiz from './components/Quiz/quiz';

ReactDOM.render(
  <Router history={browserHistory}>
        <Route path="/" component={MainView}>
          <IndexRoute component={InitialForm}/>
          <Route path="/quiz/:id/:username" component={Quiz}/>
        </Route>
  </Router>,
  document.getElementById('root')
);
